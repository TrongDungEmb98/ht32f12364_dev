/*
 * template.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "timerUser.h"
#include "uartUser.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */
#define TM_FREQ_HZ 1000 
/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */



/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
static volatile uint16_t tmUpdateCount = 0;
 
/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
void Timer0_Configuration(void)
{
    CKCU_ClocksTypeDef ClockFreq;
    CKCU_GetClocksFrequency(&ClockFreq);
    DEBUG("ClockFreq.HCLK_Freq: %d\r\n", ClockFreq.HCLK_Freq);
    
    { /* Enable peripheral clock                                                                              */
    CKCU_PeripClockConfig_TypeDef CKCUClock = {{ 0 }};
    CKCUClock.Bit.GPTM0 = 1;
    CKCU_PeripClockConfig(CKCUClock, ENABLE);
    }

    { /* Time base configuration                                                                              */
			/* !!! NOTICE !!!
					Notice that the local variable (structure) did not have an initial value.
					Please confirm that there are no missing members in the parameter settings below in this function.
			*/
			TM_TimeBaseInitTypeDef TimeBaseInit;

			TimeBaseInit.Prescaler = ClockFreq.HCLK_Freq / 1000000 - 1;   // Timer clock = CK_AHB / 1000000 => 1MHz
			TimeBaseInit.CounterReload = TM_FREQ_HZ - 1;									// update time = TM_FREQ_HZ / 1MHz
			/* update event Hz = ClockFreq.HCLK_Freq / Prescaler / CounterReload 
														= ClockFreq.HCLK_Freq / (ClockFreq.HCLK_Freq / 1000000) / (TM_FREQ_HZ)
															= 1000000 / TM_FREQ_HZ */
			TimeBaseInit.RepetitionCounter = 0;
			TimeBaseInit.CounterMode = TM_CNT_MODE_DOWN;
			TimeBaseInit.PSCReloadTime = TM_PSC_RLD_IMMEDIATE;
			TM_TimeBaseInit(HT_GPTM0, &TimeBaseInit);

			/* Clear Update Event Interrupt flag since the "TM_TimeBaseInit()" writes the UEV1G bit                 */
			TM_ClearFlag(HT_GPTM0, TM_FLAG_UEV);
    }

    /* Enable Update Event interrupt                                                                          */
    NVIC_EnableIRQ(GPTM0_IRQn);
    TM_IntConfig(HT_GPTM0, TM_INT_UEV, ENABLE);

    TM_Cmd(HT_GPTM0, ENABLE);
}

void GPTM0_IRQHandler(void)
{
  TM_ClearFlag(HT_GPTM0, TM_INT_UEV);
  tmUpdateCount++;
}

void Timer0_Test(void)
{
	if(tmUpdateCount >= 1000)
	{
		tmUpdateCount = 0;
		DEBUG("Test timer0\r\n");
	}
}