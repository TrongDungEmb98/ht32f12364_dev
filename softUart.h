/*
 * softUart.h
 *
 *  Created on: Dec 8, 2020
 *      Author: chungnt@epi-tech.com.vn
 */

#ifndef SOFTUART_H_
#define SOFTUART_H_
#include "ht32.h"

#define MAX_RX_LEN 100

typedef enum
{
	SWUART_Data_BitStart = 8,
	SWUART_Data_Bit0 = 7,
	SWUART_Data_Bit1 = 6,
	SWUART_Data_Bit2 = 5,
	SWUART_Data_Bit3 = 4,
	SWUART_Data_Bit4 = 3,
	SWUART_Data_Bit5 = 2,
	SWUART_Data_Bit6 = 1,
	SWUART_Data_Bit7 = 0,
	SWUART_Data_BitStop = -1,
} SWUART_Data_Bits;

typedef enum
{
	SWUART_State_Buffer_Empty = 0,
	SWUART_State_Buffer_Frame,
	SWUART_State_Buffer_Full,
} SWUART_State_Buffer_t;

typedef struct
{
	uint8_t byteRX;
	uint8_t bufferRX[MAX_RX_LEN];
	uint8_t bufferIndex;
	SWUART_State_Buffer_t state;
} SWUART_Typedef_t;

extern volatile SWUART_Typedef_t DataRX_t;

void delay_us(unsigned long time);					// delay microseconds
void SOFTUART_Configuration(void);
void SOFTUART_TransmitChar(const char DataValue);
void SOFTUART_TransmitString(const char* str);
void SOFTUART_TransmitStringValue(const char* str, ...);
#endif /* SOFTUART_SOFTUART_H_ */
