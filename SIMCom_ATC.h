#ifndef _SIMCOM_ATC_H
#define _SIMCOM_ATC_H
#include "ht32.h"
#include <stdbool.h>

#define SMALL_BUFFER_SIZE 200

typedef enum {
    EVEN_OK = 0,  // SIMCom_ATC response dung
    EVEN_TIMEOUT, // Het timeout ma chua co response
    EVEN_ERROR,   // SIMCom_ATC response ko dung
} SIMCom_ATC_ResponseEvent_t;

typedef enum {
    SIMCom_ATC_OK = 0,
    SIMCom_ATC_RESET = 1,
    SIMCom_ATC_SENSMS = 2,
    SIMCom_ATC_READSMS = 3,
    SIMCom_ATC_POWERON = 4,
    SIMCom_ATC_REOPENPPP = 5,
    SIMCom_ATC_GETBTSINFOR = 6,
    SIMCom_ATC_SENDATC = 7
} SIMCom_ATC_State_t;

typedef enum {
    SIMCom_ATC_AT_MODE = 1,
    SIMCom_ATC_PPP_MODE
} SIMCom_ATC_Mode_t;

typedef void (*SIMCom_ATC_SendATCallBack_t)(SIMCom_ATC_ResponseEvent_t event, void *ResponseBuffer);

typedef struct{		
    SIMCom_ATC_State_t State;
    SIMCom_ATC_Mode_t	Mode;
    uint8_t Step;
    uint8_t RISignal;
    uint8_t Dial;
    uint8_t GetBTSInfor;
    uint8_t SIMCom_ATCReady;
    uint8_t FirstTimePower;
    uint8_t SendSMSAfterRead;
    uint8_t PPPCommandState;
    uint16_t TimeOutConnection;
    uint16_t TimeOutCSQ;
    uint8_t TimeOutOffAfterReset;
    uint8_t isSIMCom_ATCOff;
}SIMCom_ATC_Manager_t;

typedef struct
{
    uint8_t Buffer[SMALL_BUFFER_SIZE];
    uint8_t BufferIndex;
    uint8_t State;
} SmallBuffer_t;

typedef struct{
	uint8_t CMD[SMALL_BUFFER_SIZE/2];
	uint8_t ExpectResponseFromATC[SMALL_BUFFER_SIZE/2];
	uint32_t TimeoutATC;
	uint32_t CurrentTimeoutATC;
	uint8_t RetryCountATC;
	SmallBuffer_t ReceiveBuffer;
	SIMCom_ATC_SendATCallBack_t SendATCallBack;
}ATCommand_t;

void SIMCom_ATC_SendATCommand(const char *Command, const char *ExpectResponse, uint32_t Timeout,
        uint8_t RetryCount, SIMCom_ATC_SendATCallBack_t CallBackFunction);

void SIMCom_ATC_Init(void);
void SIMCom_ATC_ManagerTask(void);
void SIMCom_ATC_ReceiveData(void);
void SIMCom_ATC_ProcessData(void);
void SIMCom_ATC_CheckTimeout(void);
#endif
