#include "SIMCom_ATC.h"
#include <stdio.h>
#include <string.h>
#include "uartUser.h"
/******************************************************************************
                                   GLOBAL VARIABLES					    			 
 ******************************************************************************/

/******************************************************************************
                                   GLOBAL FUNCTIONS					    			 
 ******************************************************************************/

/******************************************************************************
                                   DATA TYPE DEFINE					    			 
 ******************************************************************************/

/******************************************************************************
                                   PRIVATE VARIABLES					    			 
 ******************************************************************************/
SIMCom_ATC_Manager_t SIMCom_ATC_Manager;
ATCommand_t SIMCom_ATC_ATCommand;
/******************************************************************************
                                   LOCAL FUNCTIONS					    			 
 ******************************************************************************/
bool CheckReadyStatus(void);
void SendATCommand(const char *cmd);
void CheckModule(SIMCom_ATC_ResponseEvent_t event, void *ResponseBuffer);
void GetBTSInfor(SIMCom_ATC_ResponseEvent_t event, void *ResponseBuffer);
void SenSMSIndicate(SIMCom_ATC_ResponseEvent_t event, void *ResponseBuffer);
void RetrySendATC(void);
void ChangeSIMCom_ATCState(SIMCom_ATC_State_t state);
void ExecuteNewSMS(uint8_t *newSMS);
/*****************************************************************************/
//bool CheckReadyStatus(void)
//{
//	return HAL_GPIO_ReadPin(SIMCom_ATC_STATUS_GPIO_Port, SIMCom_ATC_STATUS_Pin);
//}

void SendATCommand(const char *cmd)
{
	UxART1_SendString((uint8_t*)cmd);	
	DEBUG("Send to SIMCom: %s", cmd);
	
}

void RetrySendATC(void)
{
	DEBUG("Retry send\n");
	SIMCom_ATC_SendATCommand((const char*)SIMCom_ATC_ATCommand.CMD, (const char*)SIMCom_ATC_ATCommand.ExpectResponseFromATC, SIMCom_ATC_ATCommand.TimeoutATC, SIMCom_ATC_ATCommand.RetryCountATC, SIMCom_ATC_ATCommand.SendATCallBack);
}

void ChangeSIMCom_ATCState(SIMCom_ATC_State_t state)
{
	SIMCom_ATC_Manager.State = state;
	SIMCom_ATC_Manager.Step = 0;
}

/*****************************************************************************/
/*
 * @brief		: Callback function 
 */
void CheckModule(SIMCom_ATC_ResponseEvent_t event, void *ResponseBuffer) 
{
	DEBUG("RSP: %s", (char*)ResponseBuffer);
	if(event == EVEN_OK) ChangeSIMCom_ATCState(SIMCom_ATC_POWERON);
}

void GetBTSInfor(SIMCom_ATC_ResponseEvent_t event, void *ResponseBuffer)
{
	if(event == EVEN_OK)
	{
		DEBUG("RSP: %s", (char*)ResponseBuffer);
	}
}

void PowerOnModuleSIMCom_ATC(SIMCom_ATC_ResponseEvent_t event, void *ResponseBuffer)
{
	if(event == EVEN_OK)
	{
		switch(SIMCom_ATC_Manager.Step)
		{
			case 1:							
				break;
			case 2:
				break;	
			case 3:	
				break;
			case 4:
				break;
		}
		SIMCom_ATC_Manager.Step++;
	}
	else 
	{
		SIMCom_ATC_Manager.Step++;
		DEBUG("Timeout\n");
	}
}

/*****************************************************************************/
/**
 * @brief		:  	init SIMCom_ATC module
 * @param		:  
 * @retval	:
 * @author	:	
 * @created	:	
 * @version	:
 * @reviewer:	
 */
void SIMCom_ATC_Init(void)
{
//	// turn on vbat
//	HAL_GPIO_WritePin(SIMCom_ATC_PWRKEY_GPIO_Port, SIMCom_ATC_PWRKEY_Pin, GPIO_PIN_RESET);
//	HAL_GPIO_WritePin(SIMCom_ATC_ONOFF_PWR_GPIO_Port, SIMCom_ATC_ONOFF_PWR_Pin, GPIO_PIN_SET);
//	HAL_Delay(1000); // cho VBAT on dinh
//	
//	// powerkey pulse
//	HAL_GPIO_WritePin(SIMCom_ATC_PWRKEY_GPIO_Port, SIMCom_ATC_PWRKEY_Pin, GPIO_PIN_SET);
//	HAL_Delay(1000);
//	HAL_GPIO_WritePin(SIMCom_ATC_PWRKEY_GPIO_Port, SIMCom_ATC_PWRKEY_Pin, GPIO_PIN_RESET);
//	HAL_Delay(1500);
//	HAL_GPIO_WritePin(SIMCom_ATC_PWRKEY_GPIO_Port, SIMCom_ATC_PWRKEY_Pin, GPIO_PIN_SET);
	
	SIMCom_ATC_Manager.FirstTimePower = 0;
}
/*****************************************************************************/
/**
 * @brief		:  	manager SIMCom_ATC call 1s/time
 * @param		:  
 * @retval	:
 * @author	:	
 * @created	:	
 * @version	:
 * @reviewer:	
 */
uint8_t u8CountGetCSQ = 0;
void SIMCom_ATC_ManagerTask(void)
{
//	if(SIMCom_ATC_Manager.FirstTimePower == 0)
//	{
//		if(CheckReadyStatus())
//		{
//			SIMCom_ATC_Manager.FirstTimePower = 1;
//			DEBUG("SIMCom_ATC_ready\n");
//			SIMCom_ATC_SendATCommand("AT\r\n", "OK", 5000, 5, CheckModule);
//			return;
//		}
//		else 
//		{
//			DEBUG("Initiating...\n");
//			return;
//		}
//	}
	
		/* Cac trang thai lam viec module SIMCom_ATC */
	switch(SIMCom_ATC_Manager.State)
	{
		case SIMCom_ATC_POWERON:
			if(SIMCom_ATC_Manager.Step == 0)
			{
				SIMCom_ATC_SendATCommand("ATV1\r\n", "OK", 1000, 1, PowerOnModuleSIMCom_ATC);
				SIMCom_ATC_Manager.Step = 1;
			}
			break;
		case SIMCom_ATC_GETBTSINFOR:		/* Get SIMCom_ATC Signel level */
			if(SIMCom_ATC_Manager.Step == 0)
			{
				SIMCom_ATC_Manager.Step = 1;
				SIMCom_ATC_SendATCommand("AT+CSQ\r\n", "OK", 1000, 3, GetBTSInfor); 
				SIMCom_ATC_Manager.State = SIMCom_ATC_OK;
			}			
			break;
		default: break;
	}
	
	if(++u8CountGetCSQ >= 10) // 10s check CSQ
	{
		u8CountGetCSQ = 0;
		ChangeSIMCom_ATCState(SIMCom_ATC_GETBTSINFOR);
	}
}

/*****************************************************************************/
/**
 * @brief		:  	send AT command
 * @param		:  	command: command to send
 * @param		:  	ExpectResponse: string Expect Response
 * @param		:  	Timeout: timeout response in ms
 * @param		:  	RetryCount: time retry cmd
 * @param		:  	CallBackFunction: callback function process cmd response
 * @retval	:
 * @author	:	
 * @created	:	
 * @version	:
 * @reviewer:	
 */
void SIMCom_ATC_SendATCommand(const char *Command, const char *ExpectResponse, uint32_t Timeout,
        uint8_t RetryCount, SIMCom_ATC_SendATCallBack_t CallBackFunction)
{
		if(Timeout == 0 || CallBackFunction == NULL)
    {
        SendATCommand(Command);
        return;
    }
		
		{
			uint16_t len = 0;
			len = sprintf((char*)SIMCom_ATC_ATCommand.CMD, "%s", Command);
			SIMCom_ATC_ATCommand.CMD[len] = 0;
			len = sprintf((char*)SIMCom_ATC_ATCommand.ExpectResponseFromATC, "%s", ExpectResponse);
			SIMCom_ATC_ATCommand.ExpectResponseFromATC[len] = 0;
			SIMCom_ATC_ATCommand.RetryCountATC = RetryCount;
			SIMCom_ATC_ATCommand.SendATCallBack = CallBackFunction;
			SIMCom_ATC_ATCommand.TimeoutATC = Timeout;
			SIMCom_ATC_ATCommand.CurrentTimeoutATC = 0;
		}
		memset(SIMCom_ATC_ATCommand.ReceiveBuffer.Buffer, 0, SMALL_BUFFER_SIZE);
		SIMCom_ATC_ATCommand.ReceiveBuffer.BufferIndex = 0;
//		SIMCom_ATC_ATCommand.ReceiveBuffer.State = BUFFER_STATE_IDLE;

		SendATCommand(Command);
}

/*****************************************************************************/
/**
 * @brief		:  	call when a new character has been received 
 * @param		:  
 * @retval	:
 * @author	:	
 * @created	:	
 * @version	:
 * @reviewer:	
 */
void SIMCom_ATC_ReceiveData(void)
{
//	RING_BUFFER_Putc(byteRx);
//	HAL_UART_Receive_IT(SIMCom_ATC_UART, &byteRx, 1);
}

/*****************************************************************************/
/**
 * @brief		:  	process AT command response
 * @param		:  
 * @retval	:
 * @author	:	
 * @created	:	
 * @version	:
 * @reviewer:	
 */
uint8_t u8HasNewSMS = 0;
void SIMCom_ATC_ProcessData(void)
{
	DEBUG("Send to SIMCom: %s", SIMCom_ATC_ATCommand.ReceiveBuffer.Buffer);
	if(strstr((const char*)SIMCom_ATC_ATCommand.ReceiveBuffer.Buffer, (const char*)SIMCom_ATC_ATCommand.ExpectResponseFromATC))
	{
		if(SIMCom_ATC_ATCommand.SendATCallBack != NULL)
		{
			SIMCom_ATC_ATCommand.TimeoutATC = 0;
			SIMCom_ATC_ATCommand.SendATCallBack(EVEN_OK, SIMCom_ATC_ATCommand.ReceiveBuffer.Buffer);
//					SIMCom_ATC_ATCommand.SendATCallBack = NULL;
		}
		else
		{
			
		}
	}
	else if(strstr((const char*)SIMCom_ATC_ATCommand.ReceiveBuffer.Buffer, "+CMT:"))
	{
		u8HasNewSMS = 1;
	}
	memset(SIMCom_ATC_ATCommand.ReceiveBuffer.Buffer, 0, SMALL_BUFFER_SIZE);
	SIMCom_ATC_ATCommand.ReceiveBuffer.BufferIndex = 0;

	SIMCom_ATC_CheckTimeout();
}

void SIMCom_ATC_CheckTimeout(void)
{
	if(SIMCom_ATC_ATCommand.TimeoutATC > 0 && SIMCom_ATC_ATCommand.CurrentTimeoutATC < SIMCom_ATC_ATCommand.TimeoutATC)
	{
		if(++SIMCom_ATC_ATCommand.CurrentTimeoutATC == SIMCom_ATC_ATCommand.TimeoutATC)
		{
			SIMCom_ATC_ATCommand.CurrentTimeoutATC = 0;
			if(SIMCom_ATC_ATCommand.RetryCountATC > 0)
			{							
				SIMCom_ATC_ATCommand.RetryCountATC--;									
				RetrySendATC();
			}
			else
			{					
				DEBUG("Callback timeout\n");
				if(SIMCom_ATC_ATCommand.SendATCallBack != NULL)
				{
					SIMCom_ATC_ATCommand.TimeoutATC = 0;
					SIMCom_ATC_ATCommand.SendATCallBack(EVEN_TIMEOUT, "@@@");
//						SIMCom_ATC_ATCommand.SendATCallBack = NULL;
				}
			}
		}
	}
}
