/*
 * w25q16jv.h
 *
 *  Created on: Sep 5, 2019
 *      Author: chungnguyen
 *
*/
#ifndef  __W25Q16JV_H__
#define  __W25Q16JV_H__
#include "ht32.h"

void W25Q16JV_readData(uint32_t add, void *rx_DATA, uint32_t len);
void W25Q16JV_writeData(uint32_t add, void *tx_DATA, uint32_t len);
void W25Q16JV_erase4k(uint32_t add);
void W25Q16JV_erase32k(uint32_t add);
void W25Q16JV_erase64k(uint32_t add);
void W25Q16JV_eraseChip(void);
uint8_t W25Q16JV_readSR1(uint8_t bitIndex);
uint8_t W25Q16JV_readSR2(uint8_t bitIndex);
uint8_t W25Q16JV_readSR3(uint8_t bitIndex);
void W25Q16JV_writeSR1(uint8_t value);
void W25Q16JV_writeSR2(uint8_t value);
void W25Q16JV_writeSR3(uint8_t value);

void W25Q16JV_selftest(void);
#endif
