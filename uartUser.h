/*
 * uartUser.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef UARTUSER_H
#define UARTUSER_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
#define MAX_NUM_DATA 100
typedef struct
{
	uint8_t Data[MAX_NUM_DATA];
	uint8_t Byte;
	uint8_t Index;
	uint8_t State;
} buffer_t;

#define DEBUG(str, ...) printf(str, ##__VA_ARGS__)
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */





/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
/*************************************************************************************************************
  * @brief  Configure the UxART
  * @retval None
  ***********************************************************************************************************/
void UxART0_Configuration(void);
void UxART1_Configuration(void);

/*************************************************************************************************************
  * @brief  Transmit data
  * @retval None
  ***********************************************************************************************************/
void UxART0_SendByte(uint8_t xByte);
void UxART0_SendString(uint8_t *str);
void UxART1_SendByte(uint8_t xByte);
void UxART1_SendString(uint8_t *str);

/*************************************************************************************************************
  * @brief  Process Received data
  * @retval None
  ***********************************************************************************************************/
void UxART0_Process(void);
void UxART1_Process(void);
#endif
#ifdef __cplusplus
}
#endif
